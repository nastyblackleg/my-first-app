package com.example.admin.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
private static ImageView imgview;
    private static Button buttonsmb;
    private int current_image;
    int[] images={R.drawable.image1,R.drawable.image2};



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
     buttonclick();
    }
        public void buttonclick()
     {
         imgview=(ImageView)findViewById(R.id.imageView);
         buttonsmb=(Button) findViewById(R.id.button);
         buttonsmb.setOnClickListener(
                 new View.OnClickListener() {
                     @Override
                     public void onClick(View v) {
                         current_image++;
                         current_image=current_image % images.length;
                         imgview.setImageResource(images[current_image ]);
                     }
                 }
         );
     }
}
